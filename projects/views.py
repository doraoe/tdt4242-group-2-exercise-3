from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, directory_path, Status
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskResponseForm, TaskPermissionForm, DeliveryForm, TeamForm, TeamAddForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from user.models import Profile
# Moved from inside method
from django.utils import timezone


def projects(request):
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request,
                  'projects/projects.html',
                  {
                      'projects': projects,
                      'project_categories': project_categories,
                  }
                  )


@login_required
def new_project(request):
    from django.contrib.sites.shortcuts import get_current_site
    current_site = get_current_site(request)
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = request.user.profile
            project.category = get_object_or_404(
                ProjectCategory, id=request.POST.get('category_id'))
            project.save()

            people = Profile.objects.filter(categories__id=project.category.id)
            from django.core import mail
            for person in people:
                if person.user.email:
                    try:
                        with mail.get_connection() as connection:
                            mail.EmailMessage(
                                "New Project: " + project.title, "A new project you might be interested in was created and can be viwed at " +
                                current_site.domain + '/projects/' +
                                str(project.id), "Beelancer", [person.user.email],
                                connection=connection,
                            ).send()
                    except Exception as e:
                        from django.contrib import messages
                        messages.success(request, 'Sending of email to ' +
                                         person.user.email + " failed: " + str(e))

            task_title = request.POST.getlist('task_title')
            task_description = request.POST.getlist('task_description')
            task_budget = request.POST.getlist('task_budget')
            for i in range(0, len(task_title)):
                Task.objects.create(
                    title=task_title[i],
                    description=task_description[i],
                    budget=task_budget[i],
                    project=project,
                )
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = 0

    for item in tasks:
        total_budget += item.budget

    if request.user == project.user.user:

        if request.method == 'POST' and 'offer_response' in request.POST:
            instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid')).status
            offer_response_form = TaskResponseForm(request.POST, instance=instance)
            if offer_response_form.is_valid():
                offer_response = offer_response_form.save(commit=False).taskoffer

            if offer_response.status.is_accepted():
                offer_response.task.read.add(offer_response.offerer)
                offer_response.task.write.add(offer_response.offerer)
                project = offer_response.task.project
                project.participants.add(offer_response.offerer)

            offer_response.status.save()
            offer_response.save()
        offer_response_form = TaskResponseForm()

        if request.method == 'POST' and 'status_change' in request.POST:
            status_form = ProjectStatusForm(request.POST)
            if status_form.is_valid():
                project_status = status_form.save(commit=False)
                project.status = project_status.status
                project.save()
        status_form = ProjectStatusForm(initial={'status': project.status})

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'status_form': status_form,
            'total_budget': total_budget,
            'offer_response_form': offer_response_form,
        })

    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            task_offer_form = TaskOfferForm(request.POST)
            if task_offer_form.is_valid():
                task_offer = task_offer_form.save(commit=False)
                task_offer.task = Task.objects.get(pk=request.POST.get('taskvalue'))
                task_offer.offerer = request.user.profile
                task_offer.status = Status.objects.create()
                task_offer.save()
        task_offer_form = TaskOfferForm()

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'task_offer_form': task_offer_form,
            'total_budget': total_budget,
        })


def is_project_owner(user, project):
    return user == project.user.user


@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = __get_user_task_permissions(request.user, task)
    accepted_task_offer = task.accepted_task_offer()

    if user_permissions['modify'] or user_permissions['write'] or user_permissions['upload'] or is_project_owner(request.user, project):
        if request.method == 'POST':
            task_file_form = TaskFileForm(request.POST, request.FILES)
            if task_file_form.is_valid():
                task_file = task_file_form.save(commit=False)
                task_file.task = task
                existing_file = task.files.filter(
                    file=directory_path(task_file, task_file.file.file)).first()
                access = user_permissions['modify'] or user_permissions['owner']
                access_to_file = False
                for team in request.user.profile.teams.all():
                    file_modify_access = TaskFileTeam.objects.filter(
                        team=team, file=existing_file, modify=True).exists()
                    print(file_modify_access)
                    access = access or file_modify_access
                access = access or user_permissions['modify']
                if access:
                    if existing_file:
                        existing_file.delete()
                    task_file.save()

                    if request.user.profile != project.user and request.user.profile != accepted_task_offer.offerer:
                        teams = request.user.profile.teams.filter(task__id=task.id)
                        for team in teams:
                            tft = TaskFileTeam()
                            tft.team = team
                            tft.file = task_file
                            tft.read = True
                            tft.save()
                else:
                    from django.contrib import messages
                    messages.warning(request, "You do not have access to modify this file")

                return redirect('task_view', project_id=project_id, task_id=task_id)

        task_file_form = TaskFileForm()
        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': task_file_form,
            }
        )
    return redirect('/user/login')


def __is_accepted_offerer(task, user):
    return task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile


def __get_user_task_permissions(user, task):
    user_permissions = {}

    is_owner = user == task.project.user.user
    is_accepted_offerer = __is_accepted_offerer(task, user)

    # Is owner or is the offerer of an accepted offer on the task
    if is_owner or is_accepted_offerer:
        user_permissions = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': is_owner,
            'upload': True,
        }

    else:
        profile = user.profile
        user_permissions = {
            'write': profile.task_participants_write.filter(id=task.id).exists(),
            'read': profile.task_participants_read.filter(id=task.id).exists(),
            'modify': profile.task_participants_modify.filter(id=task.id).exists(),
            'owner': False,
            # Team members can view its teams tasks
            'view_task': profile.teams.filter(task__id=task.id).exists(),
            'upload': profile.teams.filter(task__id=task.id, write=True).exists(),
        }

    return user_permissions


@login_required
def task_view(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)

    if not __can_view_task(request.user, task):
        return redirect('/user/login')

    deliver_form = DeliveryForm()
    deliver_response_form = TaskResponseForm()
    team_form = TeamForm()
    team_add_form = TeamAddForm()

    deliveries = task.delivery.all()
    team_files = []
    per = {}
    for f in task.files.all():
        per[f.name()] = {}
        for p in f.teams.all():
            per[f.name()][p.team.name] = p
            if p.read:
                team_files.append(p)

    return render(request, 'projects/task_view.html', {
        'task': task,
        'project': project,
        'user_permissions': __get_user_task_permissions(user, task),
        'deliver_form': deliver_form,
        'deliveries': deliveries,
        'deliver_response_form': deliver_response_form,
        'team_form': team_form,
        'team_add_form': team_add_form,
        'team_files': team_files,
        'per': per
    })


def __get_user_project_task(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)

    return (user, project, task)


def __can_view_task(user, task):
    user_permissions = __get_user_task_permissions(user, task)
    return user_permissions['read'] or user_permissions['write'] or user_permissions['modify'] or user_permissions['owner'] or user_permissions['view_task']


def task_deliver(request, project_id, task_id):
    (user, project, task) = __get_user_project_task(request, project_id, task_id)

    if request.POST and __is_accepted_offerer(task, user):
        deliver_form = DeliveryForm(request.POST, request.FILES)
        if deliver_form.is_valid():
            delivery = deliver_form.save(commit=False)
            delivery.task = task
            delivery.delivery_user = user.profile
            delivery.status = Status.objects.create()
            delivery.save()
            task.status = "pa"
            task.save()

    return task_view(request, project_id, task_id)


def task_delivery_response(request, project_id, task_id):
    (user, project, task) = __get_user_project_task(request, project_id, task_id)

    if request.POST:
        instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id')).status
        deliver_response_form = TaskResponseForm(request.POST, instance=instance)
        if deliver_response_form.is_valid():
            delivery = deliver_response_form.save().delivery

            delivery.responding_time = timezone.now()
            delivery.responding_user = user.profile
            delivery.save()

            if delivery.status.is_accepted():
                task.status = "pp"
                task.save()
            elif delivery.status.is_declined():
                task.status = "dd"
                task.save()

    return redirect('task_view', project_id=project_id, task_id=task_id)


def task_team_add(request, project_id, task_id):
    (user, project, task) = __get_user_project_task(request, project_id, task_id)

    if request.POST and __is_accepted_offerer(task, user):
        instance = get_object_or_404(Team, id=request.POST.get('team-id'))
        team_add_form = TeamAddForm(request.POST, instance=instance)
        if team_add_form.is_valid():
            team = team_add_form.save(False)
            team.members.add(*team_add_form.cleaned_data['members'])
            team.save()

    return redirect('task_view', project_id=project_id, task_id=task_id)


# Create a role
def task_team(request, project_id, task_id):
    (user, project, task) = __get_user_project_task(request, project_id, task_id)

    if request.POST and __is_accepted_offerer(task, user):
        team_form = TeamForm(request.POST)
        if team_form.is_valid():
            team = team_form.save(False)
            team.task = task
            team.save()

    return redirect('task_view', project_id=project_id, task_id=task_id)


def __post_task_permissions(request, project_id, task_id):
    (user, project, task) = __get_user_project_task(request, project_id, task_id)

    for t in task.teams.all():
        for f in task.files.all():
            try:
                tft_string = 'permission-perobj-' + str(f.id) + '-' + str(t.id)
                tft_id = request.POST.get(tft_string)
                instance = TaskFileTeam.objects.get(id=tft_id)
            except Exception as e:
                instance = TaskFileTeam(
                    file=f,
                    team=t,
                )

            instance.read = request.POST.get(
                'permission-read-' + str(f.id) + '-' + str(t.id)) or False
            instance.write = request.POST.get(
                'permission-write-' + str(f.id) + '-' + str(t.id)) or False
            instance.modify = request.POST.get(
                'permission-modify-' + str(f.id) + '-' + str(t.id)) or False
            instance.save()

        t.write = request.POST.get('permission-upload-' + str(t.id)) or False
        t.save()

def __apply_permissions(request, task, task_permission_form):
    try:
        username = task_permission_form.cleaned_data['user']
        user = User.objects.get(username=username)
        permission_type = task_permission_form.cleaned_data['permission']
        if permission_type == 'Read':
            task.read.add(user.profile)
        elif permission_type == 'Write':
            task.write.add(user.profile)
        elif permission_type == 'Modify':
            task.modify.add(user.profile)
    except Exception:
        print("user not found")


@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    if request.POST and 'permissions' in request.POST:
        __post_task_permissions(request, project_id, task_id)
        return redirect('task_view', project_id=project_id, task_id=task_id)

    accepted_task_offer = task.accepted_task_offer()
    is_owner = project.user == request.user.profile
    try:
        is_accepted_offerer = user == accepted_task_offer.offerer.user
    except AttributeError:
        is_accepted_offerer = False
    is_task_of_project = int(project_id) == task.project.id
    if (is_owner or is_accepted_offerer) and is_task_of_project:
        if request.method == 'POST':
            task_permission_form = TaskPermissionForm(request.POST)
            if task_permission_form.is_valid():
                __apply_permissions(request, task, task_permission_form)
                return redirect('task_view', project_id=project_id, task_id=task_id)

        task_permission_form = TaskPermissionForm()
        return render(
            request,
            'projects/task_permissions.html',
            {
                'project': project,
                'task': task,
                'form': task_permission_form,
            }
            )
    return redirect('task_view', project_id=project_id, task_id=task_id)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
