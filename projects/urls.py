from django.urls import path
from . import views

urlpatterns = [
    path('', views.projects, name='projects'),
    path('new/', views.new_project, name='new_project'),
    path('<project_id>/', views.project_view, name='project_view'),
    path('<project_id>/tasks/<task_id>/', views.task_view, name='task_view'),
    path('<project_id>/tasks/<task_id>/deliver/', views.task_deliver, name='task_deliver'),
    path('<project_id>/tasks/<task_id>/team_add/', views.task_team_add, name='task_team_add'),
    path('<project_id>/tasks/<task_id>/team/', views.task_team, name='task_team'),
    path('<project_id>/tasks/<task_id>/delivery_response/', views.task_delivery_response, name='task_delivery_response'),
    path('<project_id>/tasks/<task_id>/upload/', views.upload_file_to_task, name='upload_file_to_task'),
    path('<project_id>/tasks/<task_id>/permissions/', views.task_permissions, name='task_permissions'),
    path('delete_file/<file_id>', views.delete_file, name='delete_file'),
]
