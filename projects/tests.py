from django.test import TestCase, Client
from .models import Project, ProjectCategory, Task, TaskOffer, Team, Delivery, TaskFileTeam, TaskFile, Status
from user.models import Profile
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile


class TaskViewTest(TestCase):

    fixtures = ['seed.json']

    def setUp(self):
        projects = []
        self.user_admin = User.objects.get(username='admin')
        self.user_harrypotter = User.objects.get(username='harrypotter')
        self.user_joe = User.objects.get(username='joe')

        self.profile_admin = Profile.objects.filter(user_id=self.user_admin.id)[0]
        self.profile_harrypotter = Profile.objects.filter(user_id=self.user_harrypotter.id)[0]
        self.profile_joe = Profile.objects.filter(user_id=self.user_joe.id)[0]

        # Make project and task
        project = Project.objects.create(
            user=self.profile_harrypotter,
            title='Test project',
            description='Test project description',
            category=ProjectCategory.objects.get(name='Gardening')
        )
        project.save()
        task = Task.objects.create(
            project=project,
            title='Test task',
            description='Test task description',
            budget=500
        )

        task.read.set([self.profile_harrypotter])
        task.write.set([self.profile_harrypotter])
        task.modify.set([self.profile_harrypotter])

        task.save()

        stat = Status.objects.create(
                status='a',
                feedback='Great!'
        )

        taskoffer = TaskOffer.objects.create(
            task=task,
            offerer=self.profile_joe,
            title='Test task offer',
            price=350,
            description='Test task offer description',
            status=stat
        )

        taskoffer.save()

        projects.append(project)
        self.client = Client(enforce_scrf_checks=False)
        self.projects = projects

    def test_if_delivery(self):
        self.client.login(username='joe', password='qwerty123')

        project = self.projects[0]
        task = project.tasks.all()[0]

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/deliver/'
        upload_file = open('static/img/honeybee.png', 'rb')
        file = SimpleUploadedFile(upload_file.name, upload_file.read())
        data = {
            'delivery': '',
            'comment': 'A comment',
            'file': file,
            'task_id': task.id
        }
        response = self.client.post(url, data)

        delivery = Delivery.objects.get(task=task, delivery_user=self.profile_joe)
        assert(delivery)
        self.assertEqual(delivery.comment, 'A comment')
        assert(delivery.file)
        self.assertEqual(Task.objects.get(pk=task.id).status, 'pa')

    def test_if_team_add(self):
        self.client.login(username='joe', password='qwerty123')

        project = self.projects[0]
        task = project.tasks.all()[0]
        team = Team.objects.create(
            name='Testing team',
            task=task,
            write=False
        )

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/team_add/'

        response = self.client.post(
            url, {'team-add': '', 'members': [self.profile_admin.id, self.profile_harrypotter.id], 'team-id': team.id})

        assert(Profile.objects.filter(id=self.profile_harrypotter.id, teams__in=[team]))
        self.assertEqual(team.members.all().count(), 2)

    def test_if_team(self):
        self.client.login(username='joe', password='qwerty123')

        project = self.projects[0]
        task = project.tasks.all()[0]

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/team/'

        response = self.client.post(url, {'team': '', 'name': 'Test team'})

        assert(Team.objects.filter(name='Test team'))

    def test_if_permissions(self):
        self.client.login(username='joe', password='qwerty123')

        project = self.projects[0]
        task = project.tasks.all()[0]
        # Add a taskFile
        upload_file = open('static/img/honeybee.png', 'rb')
        task_file = TaskFile.objects.create(
            task=task, file=SimpleUploadedFile(upload_file.name, upload_file.read()))
        task.files.add(task_file)

        # Add a team
        team = Team.objects.create(name='Test team', task=task)
        task.teams.add(team)
        team.members.add(self.profile_joe.id)
        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/permissions/'

        team_file_string = str(task_file.id) + '-' + str(team.id)
        data = {
            'permissions': '',
            'permission-perobj-' + team_file_string: -1,
            "permission-read-" + team_file_string: True,
            "permission-write-" + team_file_string: True,
            "permission-upload-" + str(team.id): True,
        }

        self.client.post(url, data)

        taskFileTeam = TaskFileTeam.objects.get(team=team)

        self.assertEqual(taskFileTeam.read, True)
        self.assertEqual(taskFileTeam.write, True)
        self.assertEqual(taskFileTeam.modify, False)
        self.assertEqual(Team.objects.get(pk=team.id).write, True)

    def test_if_delivery_response_accept(self):
        self.client.login(username='harrypotter', password='qwerty123')

        project = self.projects[0]
        task = project.tasks.all()[0]

        upload_file = open('static/img/honeybee.png', 'rb')
        file = SimpleUploadedFile(upload_file.name, upload_file.read())

        stat = Status.objects.create(
                status='a',
                feedback=''
        )

        delivery = Delivery.objects.create(
            comment='A comment', file=file, task=task, status=stat, delivery_user=self.profile_joe)
        delivery.save()

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/delivery_response/'

        data = {
            'delivery-response': '',
            'feedback': 'Good!',
            'delivery-id': delivery.id,
            'status': 'a',
        }

        self.client.post(url, data)

        delivery = Delivery.objects.get(pk=delivery.id)
        task = Task.objects.get(pk=task.id)
        self.assertEqual(delivery.responding_user, self.profile_harrypotter)
        self.assertEqual(task.status, 'pp')

    def test_if_delivery_response_reject(self):
        self.client.login(username='harrypotter', password='qwerty123')

        project = self.projects[0]
        task = project.tasks.all()[0]

        upload_file = open('static/img/honeybee.png', 'rb')
        file = SimpleUploadedFile(upload_file.name, upload_file.read())

        stat = Status.objects.create(
                status='a',
                feedback=''
        )

        delivery = Delivery.objects.create(
            comment='A comment', file=file, task=task, status=stat, delivery_user=self.profile_joe)
        delivery.save()

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/delivery_response/'

        data = {
            'delivery-response': '',
            'feedback': 'Good!',
            'delivery-id': delivery.id,
            'status': 'd'
        }

        self.client.post(url, data)

        delivery_after = Delivery.objects.get(pk=delivery.id)
        task_after = Task.objects.get(pk=task.id)
        self.assertEqual(delivery_after.responding_user, self.profile_harrypotter)
        self.assertEqual(task_after.status, 'dd')

    def test_if_can_not_view_task(self):
        self.client.login(username='admin', password='qwerty123')
        project = self.projects[0]
        task = project.tasks.all()[0]

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/'

        response = self.client.get(url)
        self.assertEqual(response.url, '/user/login')
        self.assertEqual(response.status_code, 302)

    def test_if_can_view_projects(self):
        self.client.login(username='admin', password='qwerty123')
        url = '/projects/'

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_task_view_return(self):
        self.client.login(username='harrypotter', password='qwerty123')
        project = self.projects[0]
        task = project.tasks.all()[0]

        # Add two taskFiles
        upload_file = open('static/img/honeybee.png', 'rb')
        task_file_1 = TaskFile.objects.create(
            task=task, file=SimpleUploadedFile(upload_file.name, upload_file.read()))
        task.files.add(task_file_1)

        task_file_2 = TaskFile.objects.create(
            task=task, file=SimpleUploadedFile(upload_file.name, upload_file.read()))
        task.files.add(task_file_2)

        # Add two teams
        team_read = Team.objects.create(name='Test team read', task=task)
        task.teams.add(team_read)
        team_read.members.add(self.profile_harrypotter.id)

        team_no_read = Team.objects.create(name='Test team two', task=task)
        task.teams.add(team_no_read)
        team_no_read.members.add(self.profile_harrypotter.id)

        # Add  taskFileTeam with read permissions
        task_file_team_read = TaskFileTeam.objects.create(file=task_file_1, team=team_read, name="Task file team", read=True, write=True)

        # Add  taskFileTeam
        task_file_team_no_read = TaskFileTeam.objects.create(file=task_file_2, team=team_no_read, name="Task file team without read", read=False, write=False)

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/'

        response = self.client.get(url)

        self.assertEqual(response.context['task'], task)
        self.assertEqual(response.context['project'], project)
        self.assertEqual(response.context['team_files'], [task_file_team_read])


    def test_do_owner_have_all_permissions(self):

        self.client.login(username='harrypotter', password='qwerty123')
        project = self.projects[0]
        task = project.tasks.all()[0]

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/'
        response = self.client.get(url)

        self.assertEqual(response.context['user_permissions']['modify'], True)
        self.assertEqual(response.context['user_permissions']['write'], True)
        self.assertEqual(response.context['user_permissions']['upload'], True)
        self.assertEqual(response.context['user_permissions']['read'], True)
        self.assertEqual(response.context['user_permissions']['owner'], True)

        self.client.logout()

    def test_if_offerer_has_correct_permisssions(self):

        self.client.login(username='joe', password='qwerty123')
        project = self.projects[0]
        task = project.tasks.all()[0]
        
        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/'
        response = self.client.get(url)

        self.assertEqual(response.context['user_permissions']['modify'], True)
        self.assertEqual(response.context['user_permissions']['write'], True)
        self.assertEqual(response.context['user_permissions']['upload'], True)
        self.assertEqual(response.context['user_permissions']['read'], True)
        self.assertEqual(response.context['user_permissions']['owner'], False)

        self.client.logout()

    def test_if_user_with_no_connection_to_project_has_no_permissions(self):

        self.client.login(username='admin', password='qwerty123')
        project = self.projects[0]
        task = project.tasks.all()[0]

        url = '/projects/' + str(project.id) + '/tasks/' + str(task.id) + '/'
        response = self.client.get(url)

        # Gets redirectes since the users can't view the task
        self.assertEqual(response.status_code, 302)

class TaskPermissionsTestCase(TestCase):
    fixtures = ['seed.json']

    def setUp(self):
        self.user_admin = User.objects.get(username='admin')
        self.user_joe = User.objects.get(username='joe')
        self.user_harry = User.objects.get(username='harrypotter')
        self.profile_admin = Profile.objects.filter(user_id=self.user_admin.id)[0]
        self.profile_joe = Profile.objects.filter(user_id=self.user_joe.id)[0]
        self.profile_harry = Profile.objects.filter(user_id=self.user_harry.id)[0]

        project = Project.objects.create(
            user=self.profile_admin,
            title='Test project',
            description='Test project description',
            category=ProjectCategory.objects.get(name='Gardening')
        )
        project.save()

        task = Task.objects.create(
            project=project,
            title='Test task',
            description='Test task description',
            budget=500
        )
        task.read.set([self.profile_admin])
        task.write.set([self.profile_admin])
        task.modify.set([self.profile_admin])
        task.save()

        self.client = Client(enforce_scrf_checks=False)
        self.project = project
        self.task = task

    def test_owner_edit_permissions(self):
        self.client.login(username='admin', password='qwerty123')
        form_data = {
            "user": self.user_joe.id,
            "permission": "Read"
        }
        response = self.client.post('/projects/' + str(self.project.id) + '/tasks/' + str(self.task.id) + '/permissions/', form_data)
        self.assertEqual(self.user_joe.profile in self.task.read.all(), True)
        self.assertEqual(response.status_code, 302)
