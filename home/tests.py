from django.test import TestCase, Client
from home.templatetags.home_extras import get_task_statuses
from projects.models import Project, Task, ProjectCategory, TaskOffer, Delivery
from user.models import Profile
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile



class GetTaskStatusesTestCase(TestCase):

    fixtures = ['seed.json',]

    def setUp(self):
        self.user_admin = User.objects.get(username='admin')

        self.profile_admin = Profile.objects.filter(user_id=self.user_admin.id)[0]

        project = Project.objects.create(
            user=self.profile_admin,
            title='Test project',
            description='Test project description',
            category=ProjectCategory.objects.get(name='Gardening')
        )
        project.save()

        task = Task.objects.create(
            project=project,
            title='Test task',
            description='Test task description',
            budget=1234
        )
        task.read.set([self.profile_admin])
        task.write.set([self.profile_admin])
        task.modify.set([self.profile_admin])
        task.save()

        self.client = Client(enforce_scrf_checks=False)
        self.project = project
        self.task = task

    def test_awaiting_delivery(self):
        result = get_task_statuses(self.project)
        self.assertEqual(result['awaiting_delivery'], 1)
        self.assertEqual(result['pending_acceptance'], 0)
        self.assertEqual(result['pending_payment'], 0)
        self.assertEqual(result['payment_sent'], 0)
        self.assertEqual(result['declined_delivery'], 0)

    def test_pending_acceptance(self):
        self.task.status = Task.PENDING_ACCEPTANCE
        self.task.save()
        result = get_task_statuses(self.project)
        self.assertEqual(result['awaiting_delivery'], 0)
        self.assertEqual(result['pending_acceptance'], 1)
        self.assertEqual(result['pending_payment'], 0)
        self.assertEqual(result['payment_sent'], 0)
        self.assertEqual(result['declined_delivery'], 0)


    def test_pending_payment(self):
        self.task.status = Task.PENDING_PAYMENT
        self.task.save()
        result = get_task_statuses(self.project)
        self.assertEqual(result['awaiting_delivery'], 0)
        self.assertEqual(result['pending_acceptance'], 0)
        self.assertEqual(result['pending_payment'], 1)
        self.assertEqual(result['payment_sent'], 0)
        self.assertEqual(result['declined_delivery'], 0)

    def test_payment_sent(self):
        self.task.status = Task.PAYMENT_SENT
        self.task.save()
        result = get_task_statuses(self.project)
        self.assertEqual(result['awaiting_delivery'], 0)
        self.assertEqual(result['pending_acceptance'], 0)
        self.assertEqual(result['pending_payment'], 0)
        self.assertEqual(result['payment_sent'], 1)
        self.assertEqual(result['declined_delivery'], 0)

    def test_declined_delivery(self):
        self.task.status = Task.DECLINED_DELIVERY
        self.task.save()
        result = get_task_statuses(self.project)
        self.assertEqual(result['awaiting_delivery'], 0)
        self.assertEqual(result['pending_acceptance'], 0)
        self.assertEqual(result['pending_payment'], 0)
        self.assertEqual(result['payment_sent'], 0)
        self.assertEqual(result['declined_delivery'], 1)
