from django import template
from projects.models import Project, Task, TaskOffer

register = template.Library()

@register.filter
def check_nr_pending_offers(project):
    pending_offers = 0
    tasks = project.tasks.all()
    for task in tasks:
        taskoffers = task.taskoffer_set.all()
        for taskoffer in taskoffers:
            if taskoffer.status.is_pending:
                pending_offers+=1
    return pending_offers


@register.filter
def check_nr_user_offers(project, user):
    offers = {}
    pending_offers = 0
    declined_offers = 0
    accepted_offers = 0
    tasks = project.tasks.all()
    for task in tasks:
        taskoffers = task.taskoffer_set.filter(offerer=user.profile)
        for taskoffer in taskoffers:
            if taskoffer.status.is_pending():
                pending_offers+=1
            elif taskoffer.status.is_accepted():
                accepted_offers+=1
            elif taskoffer.status.is_declined():
                declined_offers+=1

    offers['declined'] = declined_offers
    offers['pending'] = pending_offers
    offers['accepted'] = accepted_offers
    return offers

@register.filter
def task_status(task):
    if task.status == Task.PENDING_ACCEPTANCE:
        return "You have deliveries waiting for acceptance"
    elif task.status == Task.PENDING_PAYMENT:
        return "You have deliveries waiting for payment"
    elif task.status == Task.PAYMENT_SENT:
        return "You have sent payment"
    return "You are awaiting delivery"


class TaskStatuses:
    def __init__(self):
        self.awaiting_delivery = 0
        self.pending_acceptance = 0
        self.pending_payment = 0
        self.payment_sent = 0
        self.declined_delivery = 0

    def get_dict(self):
        return {
            'awaiting_delivery': self.awaiting_delivery,
            'pending_acceptance': self.pending_acceptance,
            'pending_payment': self.pending_payment,
            'payment_sent': self.payment_sent,
            'declined_delivery': self.declined_delivery
        }


@register.filter
def get_task_statuses(project):
    task_statuses = TaskStatuses()
    tasks = project.tasks.all()
    for task in tasks:
        if task.status == Task.AWAITING_DELIVERY:
            task_statuses.awaiting_delivery += 1
        elif task.status == Task.PENDING_ACCEPTANCE:
            task_statuses.pending_acceptance += 1
        elif task.status == Task.PENDING_PAYMENT:
            task_statuses.pending_payment += 1
        elif task.status == Task.PAYMENT_SENT:
            task_statuses.payment_sent += 1
        elif task.status == Task.DECLINED_DELIVERY:
            task_statuses.declined_delivery += 1

    return task_statuses.get_dict()

@register.filter
def all_tasks(project):
    return project.tasks.all()

@register.filter
def offers(task):
    task_offers = task.taskoffer_set.all()
    msg = "No offers"
    if len(task_offers) > 0:
        x = 0
        msg = "You have "
        for t in task_offers:
            x+=1
            if t.status.is_accepted():
                return "You have accepted an offer for this task"
        msg += str(x) + " pending offers"
    return msg

@register.filter
def get_user_task_statuses(project, user):
    task_statuses = {}

    awaiting_delivery = 0
    pending_acceptance = 0
    pending_payment = 0
    payment_sent = 0
    declined_delivery = 0

    tasks = project.tasks.all()
    task_offer = None

    for task in tasks:
        try:
            taskoffers = task.taskoffer_set.all()
            for t in taskoffers:
                if t.status.is_accepted():
                    task_offer = t
                    break

            if task_offer.offerer == user.profile:
                if task.status == Task.AWAITING_DELIVERY:
                    awaiting_delivery+=1
                elif task.status == Task.PENDING_ACCEPTANCE:
                    pending_acceptance+=1
                elif task.status == Task.PENDING_PAYMENT:
                    pending_payment+=1
                elif task.status == Task.PAYMENT_SENT:
                    payment_sent+=1
                elif task.status == Task.DECLINED_DELIVERY:
                    declined_delivery+=1

        except (TaskOffer.DoesNotExist, AttributeError):
            pass


    task_statuses['awaiting_delivery'] = awaiting_delivery
    task_statuses['pending_acceptance'] = pending_acceptance
    task_statuses['pending_payment'] = pending_payment
    task_statuses['payment_sent'] = payment_sent
    task_statuses['declined_delivery'] = declined_delivery

    return task_statuses
